var crypto = require('crypto'),
  _ = require('underscore'),
  url = require('url');

var authenticate = require("authenticate");
var MongoClient = require('mongodb');
var format = require('util').format;
var host = process.env['MONGO_NODE_DRIVER_HOST'] != null ? process.env['MONGO_NODE_DRIVER_HOST'] : 'localhost';
var port = process.env['MONGO_NODE_DRIVER_PORT'] != null ? process.env['MONGO_NODE_DRIVER_PORT'] : 27017;
var db;

// Initialize by running apiauth.middleware(options);
var apiauth = module.exports = {};

apiauth.defaults = {}

apiauth.initialize = function(options) {
  this.options = options || {};
  _.defaults(this.options, this.defaults);

  apiauth.enforceToken = this.options.config.enforceToken;

  authenticate.middleware({
    encrypt_key: this.options.encrypt_key,
    validate_key: this.options.validate_key 
  });

  if(!db && this.options.db) {
      db = this.options.db;
    } else {
      // make connection
      MongoClient.connect(format("mongodb://%s:%s/pictor?w=1", host, port), function(err, database) {
        if(err) {
          res.write('{error:"mongodb is unavailable"}');
          res.end();
          return;
        }

        db = database;
      });
    }

  return(apiauth);
}

apiauth.check = function() {
  var self = this;

  var auth = function(req, res, next) {

    var parsed_url = url.parse(req.url, true);
    var apikey = req.body.p_apikey || parsed_url.query.p_apikey || req.headers["x-p_apikey"];
    var signature = req.body.p_signature || parsed_url.query.p_signature || req.headers['x-p_signature'];
    req.apiAccess = {
      "apikey": apikey,
      "signature": signature
    };

    if(!this.enforceToken)
        return next();

    checkToken(apikey, signature, req, res, next);
  };
  
  return auth;
}

// PRIVATE METHODS

// need to have a private and public key
// http://localhost:3000/api/getimage/TheSha256Hash/?w=200&border_s=10&apikey=thekey

//Signature = URL-Encode( Base64( HMAC-SHA1( YourSecretAccessKeyID, UTF-8-Encoding-Of( StringToSign ) ) ) );
// GET /photos/puppy.jpg?AWSAccessKeyId=AKIAIOSFODNN7EXAMPLE&
    // Signature=NpgCjnDzrM%2BWFzoENXmpNDUsSn8%3D&
    // Expires=1175139620 HTTP/1.1

var checkSignature = function (signature, message, key) {

  // console.log(decodeURIComponent(message));
  var hash = crypto.createHmac('SHA256', key).update(decodeURIComponent(message)).digest('base64');
  var dSignature = decodeURIComponent(signature);
  // console.log(hash)
  // console.log(dSignature)

  if(hash === dSignature)
    return true;
  return false;
}    

// {
//   "token": {
//     "_id": "534e9fff56c104a1796ed764",
//     "email": "",
//     "name": "test company",
//     "baseurl": "localhost:3000",
//     "apikey": "7141C757-AEEE-4D12-B87B-60444EEB4D29",
//     "secret": "A58D2A0C-2820-4DEC-A709-007D1C7B1EE7",
//     "token": "bEVIZzdHVHJwZzBQTXBFa1JUVkU0V291WUEwPW1ReHZVVE9OYzlmNjg5NTk2ZjcwODVhMjUwMmFmYWYxMjlkNWUxMTk4NGQ4MDczY2MxYWVkODQ4MTQ5MDU3ZGI3ZDEzMzc4MzA2ZTAwNDNkNzY0Zjc0NDExZTYxY2Q1OWQ3Mjc2NGNmYTIxNzZlNzQyMGYwMjMzYWEwMTFkMDEwZDM1ZDk3MmE5NmE4MjBkOWI4ZThiNGFkZjY5MzU2YWJhZjlmOGU4N2NiMTMzM2I4YTkxNTBjMjhmMTI2ODlhY2FkMzg2OWRmMjY5ZDY4NDczMGJlYjU5NmEyYTU0MTFmNDNkNjc4Njc"
//   }
// }
// PMLNnZRrLunDNU82iD8arctTmUpBa9JZpGnv4Z%2BDF4M%3D

// http://localhost:3000/api/getimage/http%3A%2F%2Fthechive.files.wordpress.com%2F2014%2F02%2Fsochi-best-humps15.jpg/?w=200&apikey=7141C757-AEEE-4D12-B87B-60444EEB4D29&signature=PMLNnZRrLunDNU82iD8arctTmUpBa9JZpGnv4Z%2BDF4M%3D


var checkToken = function(apikey, signature, req, res, next) {
  var collection = db.collection('tokens');
  var demoSignature = req.app.settings.demoSignature;
  //checkSignature(signature, 'http://thechive.files.wordpress.com/2014/02/sochi-best-humps15.jpg', '00000000-0000-0000-0000-000000000000');
  // var timestamp = req.params.timestamp
  // if(timestamp && timestamp )

  collection.find({'apikey': apikey}).nextObject(function(err, doc) {         
      if(doc !== null) {
        
        var message = req.params.image || req.params.timestamp;

        if( doc.secret && checkSignature(signature, message, doc.secret) ) {
          return next();
        }

        // var deserialized_token = authenticate.deserializeToken(doc.token);
        // console.log(deserialized_token)
        // if(deserialized_token) {
        //   var url = deserialized_token[1];
        //   var extra_data = deserialized_token[3];
        //   req.isAuthorized = url === req.headers.host;
        //   return next();
        // } else {
        //   res.write('{error:"API Key required"}');
        //   res.end();
        //   return;
        // }
      } else {
        if(checkSignature(signature, req.params.image, '00000000-0000-0000-0000-000000000000'))
          return next();
      }

      // if(signature === demoSignature)
      //   return next();

      res.status(401);
      res.write('{error:"API Key required", requestedImage: "' + decodeURIComponent(req.params.image) + '"}');
      res.end();
      return;
  });
}
