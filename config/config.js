module.exports = {
  db: 'localhost',
  mongodbHost: 'localhost',
  mongodbPort: '27017',
  memcachedServer: 'localhost',
  imageStore: './img_cache/',
  demoImage: 'http://upload.wikimedia.org/wikipedia/commons/6/64/2006-07-14-Denver_Skyline_Midnight.jpg',
  enforceToken: true
};
