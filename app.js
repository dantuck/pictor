/**
 * Module dependencies.
 */

var express = require('express');
//var routes = require('./routes');
var flash = require('express-flash');
var path = require('path');
var passport = require('passport');
var expressValidator = require('express-validator');
var MongoClient = require('mongodb');
var crypto = require('crypto');

/**
 * API keys + Passport configuration.
 */

var secrets = require('./config/secrets');
var passportConf = require('./config/passport');
var config = require('./config/config');

if( typeof process.env['MONGO_NODE_DRIVER_HOST'] === 'undefined' ) {
  process.env['MONGO_NODE_DRIVER_HOST'] = config.mongodbHost ? config.mongodbHost : 'localhost';
}

if( typeof process.env['MONGO_NODE_DRIVER_PORT'] === 'undefined'  ) {
  process.env['MONGO_NODE_DRIVER_PORT'] = config.mongodbPort ? config.mongodbPort : 27017;
}

var format = require('util').format;
var host = process.env['MONGO_NODE_DRIVER_HOST'] != null ? process.env['MONGO_NODE_DRIVER_HOST'] : 'localhost';
var port = process.env['MONGO_NODE_DRIVER_PORT'] != null ? process.env['MONGO_NODE_DRIVER_PORT'] : 27017;

var apiauth = require("./middleware/apiauth");
/**
 * Load controllers.
 */

var homeController = require('./controllers/home');
var apiController = require('./controllers/api');
var tokenController = require('./controllers/token');

/**
 * Create Express server.
 */

var app = express();

/**
 * Express configuration.
 */

var hour = 3600000;
var day = (hour * 24);
var week = (day * 7);
var month = (day * 30);

app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
// app.set('demoSignature', 'PMLNnZRrLunDNU82iD8arctTmUpBa9JZpGnv4Z+DF4M=')
app.use(require('connect-assets')({
  src: 'public',
  helperContext: app.locals
}));

app.use(express.compress());
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.cookieParser());
app.use(express.json());
app.use(express.urlencoded());
app.use(expressValidator());
app.use(express.methodOverride());
app.use(express.session({
  secret: secrets.sessionSecret
}));
app.use(passport.initialize());
app.use(passport.session());

app.use(flash());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public'), { maxAge: week }));
app.use(function(req, res) {
  res.status(404);
  res.render('404');
});
app.use(express.errorHandler());

/**
 * Application routes.
 */

//routes.attachHandlers(app);

app.get('/', homeController.index);
app.get('/getstarted', homeController.getStarted);
app.get('/token', tokenController.index);
app.get('/token/stats/:apikey?', tokenController.stats);
app.post('/token/request', tokenController.requestToken);

/**
 * API routes.
 */

app.get('/api', apiController.getApi);
app.get('/api/v1', apiController.getApi);
app.get('/api/getimage/:image?', apiauth.check(), apiController.getImage);
app.get('/api/v1/getimage/:image?', apiauth.check(), apiController.getImage);

app.get('/api/demo/getimage/', apiController.getImageDemo);
app.get('/api/v1/demo/getimage/', apiController.getImageDemo);
app.get('/api/preview/:url?', apiController.getLinkPreview);

/**
 * Start Express server.
 */

MongoClient.connect(format("mongodb://%s:%s/pictor?w=1", host, port), function(err, database) {
  if(err) {
    console.error('✗ MongoDB Connection Error. Please make sure MongoDB is running.');
    throw err;
  }

  app.db = database;

  app.use(apiauth.initialize({
    encrypt_key: secrets.auth_encrypt_key,
    validate_key: secrets.auth_validate_key,
    db: app.db,
    config: config
  }));

  var demoSignature = crypto.createHmac('SHA256', '00000000-0000-0000-0000-000000000000').update(config.demoImage).digest('base64');
  app.set('demoSignature', demoSignature);

  app.listen(app.get('port'), function() {
    console.log("✔ Express server listening on port %d in %s mode", app.get('port'), app.settings.env);
  });

});


