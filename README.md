Pictor
=================

Table of Contents
-----------------
- [Features](#markdown-header-features)
- [Prerequisites](#markdown-header-prerequisites)
- [Getting Started](#markdown-header-getting-started)
- [Project Structure](#markdown-header-project-structure)
- [List of Packages](#markdown-header-list-of-packages)
- [Useful Tools](#markdown-header-useful-tools)
- [Recommended Node.js Libraries](#markdown-header-recommended-nodejs-libraries)
- [Pro Tips](#markdown-header-pro-tips)
- [FAQ](#markdown-header-faq)
- [How It Works](#markdown-header-how-it-works-mini-guides)
- [Deployment](#markdown-header-deployment)
- [TODO](#markdown-header-todo)
- [Contributing](#markdown-header-contributing)
- [License](#markdown-header-license)

## Features
--------
- Demo site
    - MVC Project Structure
    - Bootstrap 3 + Flat UI + iOS7 Theme
- Node.js clusters support

## Prerequisites
-------------
- [GraphicsMagick](http://www.graphicsmagick.org/README.html)
- [Node.js](http://nodejs.org)
- [Memcached](http://memcached.org/)
- [MongoDB](https://www.mongodb.org/)

> **Note**: If you are new to Node.js or Express framework, I highly recommend watching [Node.js and Express 101](http://www.youtube.com/watch?v=BN0JlMZCtNU) screencast by Alex Ford that teaches Node and Express from scratch. Alternatively, here is another great tutorial for complete beginners - [Getting Started With Node.js, Express, MongoDB](http://cwbuecheler.com/web/tutorials/2013/node-express-mongo/).


## Getting Started
---------------

The easiest way to get started is to clone the repository:

```bash
# Fetch only the latest commits
git clone --depth=1 git@bitbucket.org:dantuck/pictor.git my-project

cd my-project

# Install NPM dependencies
npm install

node app.js

```



> **Note**: I strongly recommend installing nodemon `sudo npm install -g nodemon`. It will monitor for any changes in your node.js application and automatically restart the server. Once installed, instead of `node app.js` use `nodemon app.js`. It will save you a lot of time in the long run, because you won't need to manually restart the server each time you make a change.

Next, if you want to use any of the included APIs or OAuth authentication methods, you will need to obtain
appropriate credentials: Client ID, Client Secret, API Key, or Username & Password. You will
need to go through each provider to generate new credentials.

## Project Structure
-----------------

| Name          | Description   |
| ------------- |:-------------:|
| **config**/passport.js      | Passport Local and OAuth strategies + Passport middleware.         |
| **config**/secrets.js    | Your API keys, tokens, passwords and database URL.                    |
| **controllers**/api.js | Controller for /api route and all api examples.                         |
| **controllers**/contact.js | Controller for contact form.                                        |
| **controllers**/home.js | Controller for home page (index).                                      |
| **controllers**/user.js | Controller for user account management page.                           |
| **models**/User.js | Mongoose schema and model for User.                                         |
| **public/***                       | Static assets (fonts, css, js, img)                         |
| **public/css**/styles.less         | Main stylesheet for your app.                               |
| **public/css/themes**/default.less | Some Bootstrap overrides to make it look prettier.          |
| **views/account/***                | Templates for *login, signup, profile*.                     |
| **views/api/***                    | Templates for API Examples.                                 |
| **views/partials**/flash.jade      | Error, info and success flash notifications.                |
| **views/partials**/navigation.jade | Navbar partial template.                                    |
| **views/partials**/footer.jade     | Footer partial template.                                    |
| **views**/layout.jade              | Base template.                                              |
| **views**/home.jade                | Home page template.                                         |
| app.js                             | Main application file.                                      |
| cluster_app.js                     | Runs multiple instances of `app.js` using <a href="http://nodejs.org/api/cluster.html" target="_blank">Node.js clusters</a>.|

**Note:** There is no preference how you name or structure your views. You could place all your templates in a top-level `views` directory without having a nested folder structure, if that makes things easier for you. Just don't forget to update `extends ../layout`  and corresponding `res.render()` method in controllers.

**Note:** Although your main template - **layout.jade** only knows about `/css/styles.css` file, you should be editing **styles.less** stylesheet. Express will automatically generate a minified **styles.css** whenever it detects changes in the *LESS* file. This is done via [connect-assets](https://github.com/adunkman/connect-assets) and [less.js](https://github.com/less/less.js).

## List of Packages
----------------
| Package       | Description   |
| ------------- |:-------------:|
| async         | Utility library that provides asynchronous control flow. |
| bcrypt-nodejs | Library for hashing and salting user passwords. |
| cheerio | Scrape web pages using jQuery-style syntax.  |
| connect-mongo | MongoDB session store for Express. |
| connect-assets | Compiles LESS stylesheets, concatenates/minifies JavaScript. |
| express | Web framework. |
| express-flash | Provides flash messages for Express. Uses connect-flash internally. |
| express-validator | Easy form validation for Express. Uses node-validator internally. |
| jade | Template engine for node.js |
| less | LESS compiler. Used implicitly by connect-assets. |
| mongoose | MongoDB object modeling tool |
| node-foursquare | Foursquare API library |
| nodemailer | Node.js library for sending emails |
| passport | Simple and elegant authentication library for node.js |
| request | Simplified HTTP request library. |
| underscore | Handy JavaScript utlities library. |
| validator | Used in conjunction with express-validator in **controllers/api.js**. |


## Useful Tools
------------
- [Jade Syntax Documentation by Example](http://naltatis.github.io/jade-syntax-docs/#attributes) - Even better than official Jade docs.
- [HTML to Jade converter](http://html2jade.aaron-powell.com) - Extremely valuable when you need to quickly copy and paste HTML snippets from the web.
- [JavascriptOO](http://www.javascriptoo.com/) - A directory of JavaScript libraries with examples, CDN links, statistics, and videos.

## Recommended Node.js Libraries
-----------------------------
- [nodemon](https://github.com/remy/nodemon) - automatically restart node.js server on code change.

## Pro Tips
--------
- If you right click and select **View Page Source**, notice how *Express*
minified HTML for you. If you would like to see non-minified markup,
add `app.locals.pretty = true;` to **app.js** with the rest of the Express configuration.

## FAQ
---

### What is `cluster_app.js`?
From the [Node.js Documentation](http://nodejs.org/api/cluster.html#cluster_how_it_works):
> A single instance of Node runs in a single thread. To take advantage of multi-core systems
> the user will sometimes want to launch a cluster of Node processes to handle the load.
> The cluster module allows you to easily create child processes that all share server ports.

`cluster_app.js` allows you to take advantage of this feature by forking a process of `app.js`
for each CPU detected. For the majority of applications serving HTTP requests,
this is a resounding boon. However, the cluster module is still in experimental stage, therefore it should only be used after understanding its purpose and behavior. To use it, simply run `node cluster_app.js`. **Its use is entirely optional and `app.js` is not tied in any way to it**. As a reminder, if you plan to use `cluster_app.js` instead of `app.js`, be sure to indicate that in `package.json` when you are ready to deploy your app.

### I am using nodemon and I am getting an error: watch ENOSPC

The error:
```
exception in nodemon killing node
Error: watch ENOSPC
    at errnoException (fs.js:1019:11)
    at FSWatcher.start (fs.js:1051:11)
    at Object.fs.watch (fs.js:1076:11)
    at Function.check (/usr/lib/node_modules/nodemon/lib/config/watchable.js:39:6)
    at alternativeCheck (/usr/lib/node_modules/nodemon/lib/config/checkWatchSupport.js:33:15)
    at checkWatchSupport (/usr/lib/node_modules/nodemon/lib/config/checkWatchSupport.js:62:5)
    at /usr/lib/node_modules/nodemon/lib/config/index.js:74:5
    at normaliseRules (/usr/lib/node_modules/nodemon/lib/config/load.js:118:3)
    at ready (/usr/lib/node_modules/nodemon/lib/config/load.js:59:9)
    at /usr/lib/node_modules/nodemon/lib/config/load.js:71:13
```
To fix it:
```
echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf && sudo sysctl -p
```

### I am getting MongoDB Connection Error, how do I fix it?
That's a custom error message defined in `app.js` to indicate that there was a connection problem to MongoDB:
```js
MongoClient.connect(format("mongodb://%s:%s/pictor?w=1", host, port), function(err, database) {
  if(err) {
    console.error('✗ MongoDB Connection Error. Please make sure MongoDB is running.');
    throw err;
  }
```
As the message says, you need to have a MongoDB server running before launching `app.js`. You can get MongoDB from
[mongodb.org/downloads](mongodb.org/downloads), or install it via a package manager
([Homebrew](http://brew.sh/) on Mac, `apt-get` on Ubuntu, `yum` on Fedora, etc.)

### I get an error when I deploy my app, why?
Chances are you haven't changed the *Dabatase URI* in `secrets.js`. If `db` is set to `localhost`, it will only work
on your machine as long as MongoDB is running. When you deploy to Heroku, OpenShift or some other provider, you will not have MongoDB
running on `localhost`. You need to create an account with [MongoLab](http://mongolab.com) or [MongoHQ](http://mongohq.com), then create a free tier database. See [Deployment](#deployment) for more information on how to
setup an account and a new database step-by-step with MongoLab.

## Deployment
----------

Once you are ready to deploy your app, you will need to create an account with a cloud platform to host it. These are not
the only choices, but they are my top picks. Create an account with **MongoLab** and then pick one of the 4 providers
below. Once again, there are plenty of other choices and you are not limited to just the ones listed below. From my
experience, **Heroku** is the easiest to get started with, it will automatically restart your node.js process when it crashes, custom domain support on free accounts and zero-downtime deployments. Or you can setup your own.




![Mongolab](http://i.imgur.com/7KnCa5a.png)
***
- Open [mongolab.com](https://mongolab.com) website
- Click the yellow **Sign up** button
- Fill in your user information then hit **Create account**
- From the dashboard, click on **:zap:Create new** button
- Select **any** cloud provider (I usually go with AWS)
- Under *Plan* click on **Single-node (development)** tab and select **Sandbox** (it's free)
 - *Leave MongoDB version as is - `2.4.x`*
- Enter *Database name** for your web app
- Then click on **:zap:Create new MongoDB deployment** button
- Now, to access your database you need to create a DB user
- You should see the following message:
 - *A database user is required to connect to this database.* **Click here** *to create a new one.*
- Click the link and fill in **DB Username** and **DB Password** fields
- Finally, in `secrets.js` instead of `db: 'localhost'`, use the following URI with your credentials:
 - `db: 'mongodb://<dbuser>:<dbpassword>@ds027479.mongolab.com:27479/<dbname>'`

> **Note**: As an alternative to MongoLab, there is also [MongoHQ](http://www.mongohq.com/home).

![heroku](http://blog.exadel.com/wp-content/uploads/2013/10/heroku-Logo-1.jpg)
***
- Download and install [Heroku Toolbelt](https://toolbelt.heroku.com/osx)
- In terminal, run `heroku login` and enter your Heroku credentials
- From *your app* directory run `heroku create`, followed by `git push heroku master`
- Done!

![Openshift](http://www.opencloudconf.com/images/openshift_logo.png)
***
- First, install this Ruby gem: `sudo gem install rhc` :gem:
- Run `rhc login` and enter your OpenShift credentials
- From *your app* directory run `rhc app create MyApp nodejs-0.10`
 - **Note**: *MyApp* is what you want to name your app (no spaces)
- Once that is done, you will be provided with **URL**, **SSH** and **Git Remote** links
- Visit that **URL** and you should see *Welcome to your Node.js application on OpenShift* page
- Copy **Git Remote** and paste it into `git remote add openshift your_git_remote`
- Before you push your app, you need to do a few modifications to your code

Add these two lines to `app.js`, just place them anywhere before `app.listen()`:
```js
var IP_ADDRESS = process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1';
var PORT = process.env.OPENSHIFT_NODEJS_PORT || 8080;
```

Then change `app.listen()` to:
```js
app.listen(PORT, IP_ADDRESS, function() {
  console.log("✔ Express server listening on port %d in %s mode", PORT, app.settings.env);
});
```
Add this to `package.json`, after *name* and *version*. This is necessary because, by default, OpenShift looks for `server.js` file. And by specifying `supervisor app.js` it will automatically restart the server when node.js process crashes.

```js
"main": "app.js",
"scripts": {
  "start": "supervisor app.js"
},
```

- Finally, now you can push your code to OpenShift by running `git push -f openshift master`
 - **Note**: The first time you run this command, you have to pass `-f` (force) flag because OpenShift creates a dummy server with the welcome page when you create a new Node.js app. Passing `-f` flag will override everything with your *Hackathon Starter* project repository. Please **do not** do `git pull` as it will create unnecessary merge conflicts.
- And you are done! (Not quite as simple as Heroku, huh?)

![nodejitsu](https://www.nodejitsu.com/img/media/nodejitsu-transparent.png)
***
- To install **jitsu**, open a terminal and type: `sudo npm install -g jitsu`
- Run `jitsu login` and enter your login credentials
- From your app directory, run `jitsu deploy`
 - This will create a new application snapshot, generate and/or update project metadata
- Done!

![Windows Azure](http://upload.wikimedia.org/wikipedia/en/f/ff/Windows_Azure_logo.png)
***
## Server Install Steps
--------------------

Ubuntu

```bash
ssh servername:22 -i loginname

sudo apt-get update
sudo apt-get install memcached git-core supervisor nginx graphicsmagick

sudo apt-get install -y python-software-properties python g++ make
sudo add-apt-repository ppa:chris-lea/node.js
sudo apt-get update
sudo apt-get install nodejs

ps aux | grep memcache

service supervisor restart

sudo mkdir /var/nodeapps

sudo nano /etc/supervisor/conf.d/sitrion-pictor.conf
```

```bash
[program:sitrion-pictor]
command = node /var/nodeapps/pictor/app.js
directory = /var/nodeapps/pictor
user = pictor
autostart = true
autorestart = true
stdout_logfile = /var/log/supervisor/sitrion-pictor.log
stderr_logfile = /var/log/supervisor/sitrion-pictor_err.log
environment = NODE_ENV="production"
```

```bash
sudo supervisorctl reread
sudo supervisorctl update

# Configure nginx
sudo service nginx start
sudo update-rc.d nginx defaults

sudo nano /etc/nginx/sites-available/sitrion-pictor
```

```bash
server {
    listen 80;
    #listen 443 ssl;
    server_name             server.address.com;
    #ssl_certificate        /etc/nginx/ssl/cert.pam;
    #ssl_certificate_key    /etc/nginx/ssl/cert.key;

    location / {
        proxy_set_header   X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header   Host      $http_host;
        proxy_set_header   X-Forwarded-Proto    $scheme;
        proxy_pass         http://127.0.0.1:3000;
    }
}
```

```bash
sudo ln -s /etc/nginx/sites-available/sitrion-pictor /etc/nginx/sites-enabled/sitrion-pictor

sudo service nginx restart
```

## TODO
----
- Pages that require login, should automatically redirect to last attempted URL on successful sign-in.

## Contributing
------------
If something is unclear, confusing, or needs to be refactored, please let me know. Pull requests are always welcome, but due to the opinionated nature of this project, I cannot accept every pull request. Please open an issue before submitting a pull request. This project uses [Airbnb JavaScript Style Guide](https://github.com/airbnb/javascript) with a few minor exceptions. If you are submitting a pull request that involves Jade templates, please make sure you are using *spaces*, not tabs.

## License
-------
The MIT License (MIT)

Copyright (c) 2014 Daniel Tucker

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
