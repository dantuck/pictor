var secrets = require('../config/secrets');
var authenticate = require("authenticate");
var uuid = require('uuid');
var db;
// var MongoClient = require('mongodb');
// var format = require('util').format;

// var host = process.env['MONGO_NODE_DRIVER_HOST'] != null ? process.env['MONGO_NODE_DRIVER_HOST'] : 'localhost';
// var port = process.env['MONGO_NODE_DRIVER_PORT'] != null ? process.env['MONGO_NODE_DRIVER_PORT'] : 27017;

/**
 * MongoClient /token
 * List of API examples.
 */

exports.index = function(req, res) {
  res.render('token/request_token', {
    identity: {
    	email: '',
    	name: '',
    	baseurl: ''
    }
  });
};

exports.stats = function(req, res) {
  var db = req.app.db;
  var collection = db.collection('token-stats');

  var key = req.params.apikey || 'demo';

  var docs = [];
  var totaltime = 0;

  collection.find({'apikey': key}).each(function(err, doc) {            
    
    if(doc !== null) {
      docs.push(doc);
      totaltime += doc.requestTime;  
    } else {
      res.json({"totalTime": totaltime,"usage": docs});
    }
  });

  
};


exports.requestToken = function(req, res) {
	var request = req.body;
	var identity = {
		email: request.email,
		name: request.name,
		baseurl: request.baseurl,
	};

	if(!db && req.app.db)
      db = req.app.db;

  	// need to check and make sure db is real

	var collection = db.collection('tokens');
	collection.find({'baseurl': request.baseurl}).nextObject(function(err, doc) {            
      if(doc === null) {
      	identity.apikey = uuid.v1();
        identity.secret = uuid.v4();
      	identity.token = authenticate.serializeToken(request.baseurl, request.email, request);
      	collection.insert(identity, function(docs) {
      		res.json({"token": identity});
      		// db.close();
      	});
      } else {
      	res.json({"token": doc});
      	// db.close();
      }

    });
};