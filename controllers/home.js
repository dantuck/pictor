/**
 * GET /
 * Home page.
 */

exports.index = function(req, res) {
  res.render('home', {
    title: 'Home'
  });
};

exports.getStarted = function(req, res) {
  res.render('getstarted', {
    title: 'Get started'
  });
};
