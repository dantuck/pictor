var request = require('request');
var cheerio = require('cheerio');
var gm = require('gm');
var fs = require('fs');
// var secrets = require('../config/secrets');
var config = require('../config/config');
var authenticate = require("authenticate");
var crypto = require('crypto');

var shasum = crypto.createHash('sha256');

var hour = 3600000;

var db;
var imageCacheCollection;

if(!fs.existsSync(config.imageStore)) {
  fs.mkdirSync(config.imageStore);
}

/**
 * GET /api
 * List of API examples.
 */

exports.getApi = function(req, res) {
  res.render('api/index', {
    title: 'API Browser'
  });
};

var handleImage = function(image, query, res, cacheKey, fileId) {
  var h, w;
  var headers = {};

  if(query.p_h || query.p_w) {
    h = query.p_h || null;
    w = query.p_w || null;
  } else {
    h = 200;
    w = 200;
  }

  // console.log('requesting image');

  var r = request.get(image);
  var iRequest = r.pipe(fs.createWriteStream(config.imageStore + fileId));

  r.on('complete', function(response) {
    headers = response.headers;

    res.set('content-type', response.headers['content-type']);
    res.set('Expires', response.headers['expires']);
    res.set('Last-Modified', response.headers['last-modified']);
    res.set('Access-Control-Allow-Credentials', '*');
    res.set('Access-Control-Allow-Origin', '*');
  });

  iRequest.on('close', function () {
    var manipulator = gm(config.imageStore + fileId);
    manipulator.autoOrient()

    manipulator.identify(function(err, info) {

      if(typeof info === 'undefined') return;

      if(typeof info.size !== 'undefined' && (info.size.width > w || info.size.height > h)) {
        manipulator.resize(w, h);
      }

      if(query.border_fs || query.p_border_w || query.p_border_c || query.p_border_fc || query.p_frame) {
        var fillSize = query.p_border_fs || 5;
        var fillColor = query.p_border_fc || 'ffffff';
        var color = query.p_border_c || 'cccccc';
        var width = query.p_border_w || 1;
        var doFrame = query.p_frame === 0 ? false : true;
        manipulator.borderColor('#' + fillColor).border(fillSize, fillSize);

        //frame
        if(doFrame) {
          manipulator.borderColor('#' + color).border(width,width);  
        }
      }

      manipulator.write(config.imageStore + fileId, function (err) {
        if (err) {
          cleanup();
          next(err);
        }

        var expireTime = new Date();
        expireTime.setTime(expireTime.getTime() + (hour * 12));

      // store in mongo
        var buffer = {"expires": expireTime, "lastModified": headers['last-modified']};
        imageCacheCollection.findAndModify({'cacheKey': cacheKey}, {}, {$set:buffer}, { 'upsert': true, 'new':true }, function(err, record) {
          //what to do on error???
          //console.log(record);
        });

        var readStream = fs.createReadStream(config.imageStore + fileId);
        readStream.on('data', function(data) {
          res.write(data);
        });

        readStream.on('end', function() {
          res.end();        
        });
      });
      
    });
  });

  iRequest.on('error', function () { 
    cleanup(); 
  });

  function cleanup() {
    //delete file and record from mongo
    imageCacheCollection.remove({'cacheKey': cacheKey});
    fs.unlink(config.imageStore + fileId);
  };
};



exports.getImage = function(req, res, next) {
  // var imgUri = 'http://thechive.files.wordpress.com/2014/04/genius-or-stupid-18.jpg';
  // var key = '78720760-9D20-42C4-B068-35453CAEB446';

  var query = req.query;
  var image = decodeURIComponent(req.params.image);
  var modifiedSince = req.get('If-Modified-Since');
  // console.log(modifiedSince);
  var cacheKey = req._parsedUrl.path;

  db = req.app.db;
  var collection = db.collection('token-stats');
  imageCacheCollection = db.collection('image-cache');

  var requestStart = Date.now();

  var stats = {
    "timeRequested": requestStart,
    "apikey": req.apiAccess.apikey || 'demo'
  };

  imageCacheCollection.find({'cacheKey': cacheKey}).nextObject(function(err, doc) {            
    var fileId;
    if(err || doc === null) {
      // no cache so make it happen
      imageCacheCollection.findAndModify({'cacheKey': cacheKey}, {}, {'cacheKey': cacheKey}, { 'upsert': true, 'new':true }, function(err, record) {
        //what to do on error???
        fileId = record._id;
        handleImage(image, query, res, cacheKey, fileId);
        stats.initRequest = true;
        writeStats(stats);
      });
      
    } else if (doc !== null) {
      fileId = doc._id;
      //figure out if it's expired
      var expiresDate = new Date(doc.expires);
      var isExpired = expiresDate < new Date();
      if(typeof modifiedSince !== 'undefined') {
        if(isExpired) {
          handleImage(image, query, res, cacheKey, fileId);
          stats.expiredContent = true;
        } else {
          // if not expired then return the 304
          stats.browserCached = true;
          res.status(304);
          res.send();
        }
        writeStats(stats);
      } else {
        // return from file system cache
        fs.exists(config.imageStore + fileId, function (exists) {
          if(exists) {
            res.set('Expires', expiresDate);
            res.set('Last-Modified', doc.lastModified);
            fs.createReadStream(config.imageStore + fileId).pipe(res);
            stats.fsCached = true;
          } else {
            handleImage(image, query, res, cacheKey, fileId);
            stats.fsRefresh = true;
          }
          writeStats(stats);
        });
      }
    }
  });
  
  
  function writeStats(stat) {
    stats.requestTime = Date.now() - requestStart;
    // record stats
    collection.insert(stats, function(docs) { });
    console.log(stats); 
  }

};

exports.getImageDemo = function(req, res, next) {
  var image = config.demoImage;
  //image = 'http://blog.freshjets.com/wp-content/uploads/2013/11/DenverColorado_0.jpg';
  var demoSignature = req.app.settings.demoSignature;
  var response = {
    image: '/api/getimage/' + encodeURIComponent(image),
    signature: 'signature=' + encodeURIComponent(demoSignature)
  };
  res.render('api/getimage', response);
  return;
};

/**
 * GET /api/preview
 * Link preview.
 */

exports.getLinkPreview = function(req, res, next) {
  var url = req.params.url || 'http://www.sitrion.com/';
  var isDemo = req.query.res === 'demo';
  request.get(url, function(err, request, body) {
    if (err) return next(err);
    var h = req.query.p_h || 100;
    var w = req.query.p_w || 100;

    var $ = cheerio.load(body);
    var images = [];
    var possibleImages = [];
    $("img").each(function() {
      var img = $(this);
      var imgAttr = {
        width: img.attr('width'),
        height: img.attr('height')
      };
      var haveSize = !(typeof imgAttr.width === 'undefined' && typeof imgAttr.height === 'undefined');
      if( !haveSize || imgAttr.width > w || imgAttr.height > h) {
        if(haveSize) {
          images.push(img.attr());
        }
        else
          possibleImages.push(img.attr());
      }
    });

    if(images.length < 3) {
      images = images.concat(possibleImages);
    }

    var response = {
      title: $("title").html(),
      images: images,
      desc: $('meta[name=description]').attr("content")
    };

    if(isDemo) {
      res.render('api/preview', response);  
    } else {
      res.json(response);
    }
  });
};
